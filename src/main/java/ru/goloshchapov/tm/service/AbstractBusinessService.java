package ru.goloshchapov.tm.service;

import ru.goloshchapov.tm.api.IBusinessRepository;
import ru.goloshchapov.tm.api.IBusinessService;
import ru.goloshchapov.tm.constant.SortConst;
import ru.goloshchapov.tm.enumerated.Sort;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.exception.empty.EmptyIdException;
import ru.goloshchapov.tm.exception.empty.EmptyNameException;
import ru.goloshchapov.tm.exception.incorrect.StatusIncorrectException;
import ru.goloshchapov.tm.exception.system.IndexIncorrectException;
import ru.goloshchapov.tm.model.AbstractBusinessEntity;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.*;

public class AbstractBusinessService<M extends AbstractBusinessEntity>
        extends AbstractService<M> implements IBusinessService<M> {

    private final IBusinessRepository<M> businessRepository;

    public AbstractBusinessService(IBusinessRepository<M> businessRepository) {
        super(businessRepository);
        this.businessRepository = businessRepository;
    }

    private void checkDateByStatus(final M model) {
        final Status status = model.getStatus();
        final Date dateStart = model.getDateStart();
        final Date dateFinish = model.getDateFinish();
        final Date dateNow = new Date();
        switch (status) {
            case COMPLETE:
                if (dateStart == null) model.setDateStart(dateNow);
                if (dateFinish == null) model.setDateFinish(dateNow);
                break;
            case IN_PROGRESS:
                if (dateStart == null) model.setDateStart(new Date());
                model.setDateFinish(null);
                break;
            default:
                model.setDateStart(null);
                model.setDateFinish(null);
        }
    }

    @Override
    public void addAll(final String userId, final Collection<M> collection) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (collection == null || collection.isEmpty()) return;
        businessRepository.addAll(userId, collection);
    }

    @Override
    public M add(final String userId, final M model) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (model == null) return null;
        return businessRepository.add(userId, model);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        return businessRepository.findAll(userId);
    }

    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (comparator == null) return null;
        return businessRepository.findAll(userId, comparator);
    }

    public List<M> findAllStarted(final String userId, final Comparator<M> comparator) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (comparator == null) return null;
        return businessRepository.findAllStarted(userId, comparator);
    }

    public List<M> findAllCompleted(final String userId, final Comparator<M> comparator) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (comparator == null) return null;
        return businessRepository.findAllCompleted(userId, comparator);
    }

    @Override
    public List<M> sortedBy(final String userId, final String sortCheck) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final Sort[] sortOptions = Sort.values();
        String sortChoice = SortConst.STATUS_DEFAULT;
        if (!isEmpty(sortCheck) && checkInclude(sortCheck,toStringArray(sortOptions))) sortChoice = sortCheck;
        final Sort sortType = Sort.valueOf(sortChoice);
        switch (sortType) {
            case DATE_START: return findAllStarted(userId, sortType.getComparator());
            case DATE_FINISH: return findAllCompleted(userId, sortType.getComparator());
            default: return findAll(userId, sortType.getComparator());
        }
    }

    @Override
    public M findOneById(final String userId, final String modelId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        return businessRepository.findOneById(userId, modelId);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        return businessRepository.findOneByIndex(userId, index);
    }

    public M findOneByName(final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return businessRepository.findOneByName(name);
    }

    @Override
    public M findOneByName(final String userId, final String name) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        return businessRepository.findOneByName(userId, name);
    }

    public boolean isAbsentByName(final String name) {
        if (isEmpty(name)) return true;
        return businessRepository.isAbsentByName(name);
    }

    public String getIdByName(final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return businessRepository.getIdByName(name);
    }

    @Override
    public int size(final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        return businessRepository.size(userId);
    }

    @Override
    public void remove(final String userId, final M model) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        businessRepository.remove(userId, model);
    }

    @Override
    public void clear(final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        businessRepository.clear(userId);
    }

    @Override
    public M removeOneById(final String userId, final String modelId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        return businessRepository.removeOneById(userId, modelId);
    }

    @Override
    public M removeOneByIndex(final String userId, final Integer index) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        return businessRepository.removeOneByIndex(userId, index);
    }

    public M removeOneByName(final String userId, final String name) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        return businessRepository.removeOneByName(userId, name);
    }

    @Override
    public M updateOneById(final String userId, final String modelId, final String name, final String description) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(modelId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        if (businessRepository.isAbsentById(modelId)) return null;
        final M model = findOneById(userId, modelId);
        model.setId(modelId);
        model.setName(name);
        model.setDescription(description);
        return model;
    }

    @Override
    public M updateOneByIndex(final String userId, final Integer index, final String name, final String description) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final int size = findAll().size();
        if (!checkIndex(index,size)) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        if (businessRepository.isAbsentByIndex(index)) return null;
        final M model = findOneByIndex(userId, index);
        model.setName(name);
        model.setDescription(description);
        return model;
    }

    public M startOneById(final String userId, final String modelId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(modelId)) throw new EmptyIdException();
        final M model = businessRepository.startOneById(userId, modelId);
        checkDateByStatus(model);
        return model;
    }

    public M startOneByIndex(final String userId, final Integer index) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final int size = businessRepository.size();
        if (!checkIndex(index,size)) throw new IndexIncorrectException();
        final M model = businessRepository.startOneByIndex(userId, index);
        checkDateByStatus(model);
        return model;
    }

    public M startOneByName(final String userId, final String name) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        final M model = businessRepository.startOneByName(userId, name);
        checkDateByStatus(model);
        return model;
    }

    public M finishOneById(final String userId, final String modelId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(modelId)) throw new EmptyIdException();
        final M model = businessRepository.finishOneById(userId,modelId);
        checkDateByStatus(model);
        return model;
    }

    public M finishOneByIndex(final String userId, final Integer index) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final int size = businessRepository.size();
        if (!checkIndex(index,size)) throw new IndexIncorrectException();
        final M model = businessRepository.finishOneByIndex(userId, index);
        checkDateByStatus(model);
        return model;
    }

    public M finishOneByName(final String userId, final String name) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        final M model = businessRepository.finishOneByName(userId, name);
        checkDateByStatus(model);
        return model;
    }

    @Override
    public M changeOneStatusById(final String userId, final String modelId, final String statusChange) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(modelId)) throw new EmptyIdException();
        if (businessRepository.isAbsentById(modelId)) return null;
        Status[] statuses = Status.values();
        if (!checkInclude(statusChange,toStringArray(statuses))) throw new StatusIncorrectException(statusChange);
        System.out.println(statusChange);
        final Status status = Status.valueOf(statusChange);
        final M model = findOneById(userId, modelId);
        model.setStatus(status);
        checkDateByStatus(model);
        return model;
    }

    @Override
    public M changeOneStatusByName(final String userId, final String name, final String statusChange) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        if (businessRepository.isAbsentByName(name)) return null;
        Status[] statuses = Status.values();
        if (!checkInclude(statusChange,toStringArray(statuses))) throw new StatusIncorrectException(statusChange);
        final Status status = Status.valueOf(statusChange);
        final M model = findOneByName(userId, name);
        model.setStatus(status);
        checkDateByStatus(model);
        return model;
    }

    @Override
    public M changeOneStatusByIndex(final String userId, final int index, final String statusChange) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final int size = businessRepository.size();
        if (!checkIndex(index, size)) throw new IndexIncorrectException();
        if (businessRepository.isAbsentByIndex(index)) return null;
        Status[] statuses = Status.values();
        if (!checkInclude(statusChange,toStringArray(statuses))) throw new StatusIncorrectException(statusChange);
        final Status status = Status.valueOf(statusChange);
        final M model = findOneByIndex(userId, index);
        model.setStatus(status);
        checkDateByStatus(model);
        return model;
    }

}
