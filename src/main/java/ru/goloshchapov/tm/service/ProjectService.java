package ru.goloshchapov.tm.service;

import ru.goloshchapov.tm.api.repository.IProjectRepository;
import ru.goloshchapov.tm.api.service.IProjectService;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.exception.empty.EmptyNameException;
import ru.goloshchapov.tm.model.Project;

import static ru.goloshchapov.tm.util.ValidationUtil.*;

public final class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(final String userId, final String name, final String description) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
        return project;
    }

}
