package ru.goloshchapov.tm.service;

import ru.goloshchapov.tm.api.repository.IProjectRepository;
import ru.goloshchapov.tm.api.service.IProjectTaskService;
import ru.goloshchapov.tm.api.repository.ITaskRepository;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.exception.empty.EmptyIdException;
import ru.goloshchapov.tm.exception.empty.EmptyNameException;
import ru.goloshchapov.tm.exception.system.IndexIncorrectException;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.model.Task;

import java.util.Date;
import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.checkIndex;
import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(final ITaskRepository taskRepository, final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public Project addProject(final String userId, final String projectName, final String projectDescription) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectName)) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(projectName);
        project.setDescription(projectDescription);
        projectRepository.add(userId, project);
        return project;
    }

    @Override
    public Task addTask(final String userId, final String taskName, final String taskDescription) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(taskName)) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(taskName);
        task.setDescription(taskDescription);
        taskRepository.add(userId, task);
        return task;
    }

    @Override
    public void clearProjects(final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        projectRepository.clear(userId);
    }

    @Override
    public List<Project> findAllProjects(final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        return projectRepository.findAll(userId);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (projectRepository.isAbsentById(projectId)) return null;
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    public boolean isEmptyProjectById(final String userId, final String projectId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectId)) return true;
        return findAllByProjectId(userId, projectId) == null;
    }

    @Override
    public List<Task> findAllByProjectName(final String userId, final String projectName) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectName)) throw new EmptyNameException();
        if (projectRepository.isAbsentByName(projectName)) return null;
        final String projectId = projectRepository.getIdByName(projectName);
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    public boolean isEmptyProjectByName(final String userId, final String projectName) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectName)) return true;
        return findAllByProjectName(userId, projectName) == null;
    }

    @Override
    public List<Task> findAllByProjectIndex(final String userId, final Integer projectIndex) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final int size = projectRepository.size();
        if (!checkIndex(projectIndex, size)) throw new IndexIncorrectException();
        if (projectRepository.isAbsentByIndex(projectIndex)) return null;
        String  projectId = projectRepository.getIdByIndex(projectIndex);
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    public boolean isEmptyProjectByIndex(final String userId, final Integer projectIndex) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final int size = projectRepository.size();
        if (!checkIndex(projectIndex, size)) return true;
        return findAllByProjectIndex(userId, projectIndex) == null;
    }

    @Override
    public Task bindToProjectById(final String userId, final String projectId, final String taskId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(taskId) || isEmpty(projectId)) throw new EmptyIdException();
        if (projectRepository.isAbsentById(projectId) || taskRepository.isAbsentById(taskId)) return null;
        return taskRepository.bindToProjectById(userId, taskId, projectId);
    }

    @Override
    public Task unbindFromProjectById(final String userId, final String projectId, final String taskId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(taskId) || isEmpty(projectId)) throw new EmptyIdException();
        if (projectRepository.isAbsentById(projectId) || taskRepository.isAbsentById(taskId)) return null;
        return taskRepository.unbindFromProjectById(userId, taskId);
    }

    @Override
    public List<Task> removeAllByProjectId(final String userId, final String projectId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (projectRepository.isAbsentById(projectId)) return null;
        return taskRepository.removeAllByProjectId(userId, projectId);
    }

    @Override
    public List<Task> removeAllByProjectName(final String userId, final String projectName) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectName)) throw new EmptyNameException();
        if (projectRepository.isAbsentByName(projectName)) return null;
        final String projectId = projectRepository.getIdByName(projectName);
        return taskRepository.removeAllByProjectId(userId, projectId);
    }

    @Override
    public List<Task> removeAllByProjectIndex(final String userId, final Integer projectIndex) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final int size = projectRepository.size();
        if (!checkIndex(projectIndex, size)) throw new IndexIncorrectException();
        if (projectRepository.isAbsentByIndex(projectIndex)) return null;
        final String projectId = projectRepository.getIdByIndex(projectIndex);
        return taskRepository.removeAllByProjectId(userId, projectId);
    }

    @Override
    public Project removeProjectById(final String userId, final String projectId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (projectRepository.isAbsentById(projectId)) return null;
        if (!isEmptyProjectById(userId, projectId)) removeAllByProjectId(userId, projectId);
        return projectRepository.removeOneById(userId, projectId);
    }

    @Override
    public Project removeProjectByName(final String userId, final String projectName) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectName)) throw new EmptyNameException();
        if (projectRepository.isAbsentByName(projectName)) return null;
        if (!isEmptyProjectByName(userId, projectName)) removeAllByProjectName(userId, projectName);
        return projectRepository.removeOneByName(userId, projectName);
    }

    @Override
    public Project removeProjectByIndex(final String userId, final Integer projectIndex) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final int size = projectRepository.size();
        if (!checkIndex(projectIndex, size)) throw new IndexIncorrectException();
        if (projectRepository.isAbsentByIndex(projectIndex)) return null;
        if (!isEmptyProjectByIndex(userId, projectIndex)) removeAllByProjectIndex(userId, projectIndex);
        return projectRepository.removeOneByIndex(userId, projectIndex);
    }

    @Override
    public void createTestData() {
        addProject("ddd","Project_1","About_1").setId("ppp111");
        addProject("ttt","Project_2","About_2").setId("ppp222");
        addProject("ttt","Project_3","About_3").setId("ppp333");
        addProject("ddd","Project_4","About_4").setId("ppp444");
        Date date = new Date();
        long time = date.getTime();
        projectRepository.findOneById("ppp111").setStatus(Status.COMPLETE);
        projectRepository.findOneById("ppp111").setDateStart(date);
        projectRepository.findOneById("ppp111").setDateFinish(new Date(time+300000));
        projectRepository.findOneById("ppp333").setStatus(Status.IN_PROGRESS);
        projectRepository.findOneById("ppp333").setDateStart(new Date(time+30000));
        addTask("ddd","Task_1","Desc_1").setId("11111");
        addTask("ddd","Task_2","Desc_2").setId("22222");
        addTask("ttt","Task_3","Desc_3").setId("33333");
        addTask("ttt","Task_4","Desc_4").setId("44444");
        addTask("ttt","Task_5","Desc_5").setId("55555");
        addTask("ttt","Task_6","Desc_6").setId("66666");
        addTask("ddd","Task_7","Desc_7").setId("77777");
        addTask("ddd","Task_8","Desc_8").setId("88888");
        addTask("ddd","Task_9","Desc_9").setId("99999");
        addTask("ttt","Task_0","Desc_0").setId("00000");
        date = new Date();
        time = date.getTime();
        bindToProjectById("ddd","ppp111","11111").setStatus(Status.IN_PROGRESS);
        taskRepository.findOneById("11111").setDateStart(new Date(time + 60000));
        bindToProjectById("ddd","ppp111","22222").setStatus(Status.NOT_STARTED);
        bindToProjectById("ttt","ppp222","33333").setStatus(Status.COMPLETE);
        taskRepository.findOneById("33333").setDateStart(new Date());
        taskRepository.findOneById("33333").setDateFinish(new Date(time + 60000));
        bindToProjectById("ttt","ppp222","44444").setStatus(Status.IN_PROGRESS);
        taskRepository.findOneById("44444").setDateStart(new Date(time + 120000));
        bindToProjectById("ttt","ppp333","55555").setStatus(Status.NOT_STARTED);
        bindToProjectById("ttt","ppp333","66666").setStatus(Status.COMPLETE);
        taskRepository.findOneById("66666").setDateStart(new Date(time + 30000));
        taskRepository.findOneById("66666").setDateFinish(new Date(time + 270000));
        bindToProjectById("ddd","ppp444","77777").setStatus(Status.COMPLETE);
        taskRepository.findOneById("77777").setDateStart(new Date(time + 300000));
        taskRepository.findOneById("77777").setDateFinish(new Date(time + 1200000));
        bindToProjectById("ddd","ppp444","88888").setStatus(Status.IN_PROGRESS);
        taskRepository.findOneById("88888").setDateStart(new Date(time + 3000));
        taskRepository.findOneById("99999").setStatus(Status.NOT_STARTED);
        taskRepository.findOneById("00000").setStatus(Status.IN_PROGRESS);
        taskRepository.findOneById("00000").setDateStart(new Date(time + 330000));
    }

}
