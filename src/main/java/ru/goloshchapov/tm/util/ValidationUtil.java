package ru.goloshchapov.tm.util;

public interface ValidationUtil {

    static boolean isEmpty(final String value) {
        return value == null || value.isEmpty();
    }

    static boolean checkIndex(final int index, final int size) {
        if (index < 0) return false;
        return index < size;
    }

    static String[] toStringArray(final Object[] objects) {
        final int length = objects.length;
        if (length == 0) return null;
        String[] stringObjects = new String[length];
        for (int i=0; i<length; i++) stringObjects[i] = objects[i].toString();
        return stringObjects;
    }

    static boolean checkInclude(final String elem, String[] set) {
        boolean check = false;
        for (String step: set) {
            check = elem.equals(step);
            if (check) break;
        }
        return check;
    }

}
