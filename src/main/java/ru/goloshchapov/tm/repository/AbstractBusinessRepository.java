package ru.goloshchapov.tm.repository;

import ru.goloshchapov.tm.api.IBusinessRepository;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.model.AbstractBusinessEntity;

import java.util.*;

public abstract class AbstractBusinessRepository<M extends AbstractBusinessEntity>
        extends AbstractRepository<M> implements IBusinessRepository<M> {

    @Override
    public void addAll(final String userId, final Collection<M> collection) {
        if (collection == null) return;
        for (final M model:collection) {
            model.setUserId(userId);
        }
        list.addAll(collection);
    }

    @Override
    public M add(final String userId, final M model) {
        model.setUserId(userId);
        list.add(model);
        return model;
    }

    @Override
    public List<M> findAll(final String userId) {
        List<M> models = new ArrayList<>();
        for (final M model:list) {
            if (!model.checkUserAccess(userId)) continue;
            models.add(model);
        }
        return models;
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        List<M> models = findAll(userId);
        if (models == null) return null;
        models.sort(comparator);
        return models;
    }

    @Override
    public List<M> findAllStarted(final String userId, final Comparator<M> comparator) {
        List<M> models = new ArrayList<>();
        for (final M model:list) {
            if (!model.checkUserAccess(userId)) continue;
            if (model.getStatus() != Status.NOT_STARTED) models.add(model);
        }
        if (models.size() == 0) return null;
        models.sort(comparator);
        return models;
    }

    @Override
    public List<M> findAllCompleted(final String userId, final Comparator<M> comparator) {
        List<M> models = new ArrayList<>();
        for (final M model:list) {
            if (!model.checkUserAccess(userId)) continue;
            if (model.getStatus() == Status.COMPLETE) models.add(model);
        }
        if (models.size() == 0) return null;
        models.sort(comparator);
        return models;
    }

    @Override
    public M findOneById(final String userId, final String modelId) {
        for (final M model:list) {
            if (!model.checkUserAccess(userId)) continue;
            if (modelId.equals(model.getId())) return model;
        }
        return null;
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        final M model = list.get(index);
        if (model.checkUserAccess(userId)) return model;
        return null;
    }

    @Override
    public M findOneByName(final String name) {
        for (final M model:list) {
            if (name.equals(model.getName())) return model;
        }
        return null;
    }

    @Override
    public M findOneByName(final String userId, final String name) {
        for (final M model:list) {
            if (!model.checkUserAccess(userId)) continue;
            if (name.equals(model.getName())) return model;
        }
        return null;
    }

    @Override
    public boolean isAbsentByName(final String name) {
        return findOneByName(name) == null;
    }

    @Override
    public String getIdByName(final String name) {
        return findOneByName(name).getId();
    }

    @Override
    public int size(final String userId) {
        return findAll(userId).size();
    }

    @Override
    public void remove(final String userId, final M model) {
        if (model.checkUserAccess(userId)) list.remove(model);
    }

    @Override
    public void clear(final String userId) {
        for (final M model:list) {
            remove(userId, model);
        }
    }

    @Override
    public M removeOneById(final String userId, final String modelId) {
        final M model = findOneById(userId, modelId);
        if (model == null) return null;
        list.remove(model);
        return model;
    }

    @Override
    public M removeOneByIndex(final String userId, final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        list.remove(model);
        return model;
    }

    @Override
    public M removeOneByName(final String userId, final String name) {
        final M model = findOneByName(userId, name);
        if (model == null) return null;
        list.remove(model);
        return model;
    }

    @Override
    public M startOneById(final String userId, final String modelId) {
        final M model = findOneById(userId, modelId);
        if (model == null) return null;
        model.setStatus(Status.IN_PROGRESS);
        model.setDateStart(new Date());
        return model;
    }

    @Override
    public M startOneByIndex(final String userId, final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        model.setStatus(Status.IN_PROGRESS);
        model.setDateStart(new Date());
        return model;
    }

    @Override
    public M startOneByName(final String userId, final String name) {
        final M model = findOneByName(userId, name);
        if (model == null) return null;
        model.setStatus(Status.IN_PROGRESS);
        model.setDateStart(new Date());
        return model;
    }

    @Override
    public M finishOneById(final String userId, final String modelId) {
        final M model = findOneById(userId, modelId);
        if (model == null) return null;
        model.setStatus(Status.COMPLETE);
        model.setDateFinish(new Date());
        return model;
    }

    @Override
    public M finishOneByIndex(final String userId, final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        model.setStatus(Status.COMPLETE);
        model.setDateFinish(new Date());
        return model;
    }

    @Override
    public M finishOneByName(final String userId, final String name) {
        final M model = findOneByName(userId, name);
        if (model == null) return null;
        model.setStatus(Status.COMPLETE);
        model.setDateFinish(new Date());
        return model;
    }

}
