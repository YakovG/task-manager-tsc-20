package ru.goloshchapov.tm.repository;

import ru.goloshchapov.tm.api.repository.ITaskRepository;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final List<Task> tasks = new ArrayList<>();
        for (final Task task: list) {
            if (!task.checkUserAccess(userId)) continue;
            if (projectId.equals(task.getProjectId())) tasks.add(task);
        }
        return tasks;
    }

    @Override
    public Task bindToProjectById(final String userId, final String taskId, final String projectId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final Task task = findOneById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindFromProjectById(final String userId, final String taskId) {
        final Task task = findOneById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public List<Task> removeAllByProjectId(final String userId, final String projectId) {
        final List<Task> tasks = findAllByProjectId(userId, projectId);
        if (tasks == null) return null;
        for (final Task task: tasks) {
            remove(userId, task);
        }
        return tasks;
    }

}
