package ru.goloshchapov.tm.repository;

import ru.goloshchapov.tm.api.repository.IUserRepository;
import ru.goloshchapov.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findUserByLogin(final String login) {
        for (final User user: list) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findUserByEmail(final String email) {
        for (final User user: list) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public final boolean isLoginExists(final String login) {
        return findUserByLogin(login) != null;
    }

    @Override
    public final boolean isEmailExists(final String email) {
        return findUserByEmail(email) != null;
    }

    @Override
    public User removeUser(final User user) {
        list.remove(user);
        return user;
    }

    @Override
    public User removeUserByLogin(final String login) {
        final User user = findUserByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

}
