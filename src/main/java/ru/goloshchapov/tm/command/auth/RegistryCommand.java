package ru.goloshchapov.tm.command.auth;

import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class RegistryCommand extends AbstractCommand {

    public static final String NAME = "registry";

    public static final String DESCRIPTION = "Registry to system";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER E-MAIL]");
        final String email = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD]");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
    }
}
