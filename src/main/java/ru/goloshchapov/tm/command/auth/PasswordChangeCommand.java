package ru.goloshchapov.tm.command.auth;

import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class PasswordChangeCommand extends AbstractCommand {

    public static final String NAME = "password-change";

    public static final String DESCRIPTION = "Set new password";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }
}
