package ru.goloshchapov.tm.command.task;

import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class TaskByProjectIdBindCommand extends AbstractTaskCommand{

    public static final String NAME = "task-bind-to-project-by-id";

    public static final String DESCRIPTION = "Add task to project by id";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ADD TASK TO PROJECT");
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getProjectTaskService().bindToProjectById(userId, projectId, taskId);
        if (task == null) throw new TaskNotFoundException();
    }
}
