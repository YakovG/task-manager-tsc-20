package ru.goloshchapov.tm.command.task;

import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println(("STATUS: " + task.getStatus().getDisplayName()));
        System.out.println("CREATED: " + task.getCreated());
        if (task.getDateStart() != null) System.out.println("STARTED: " + task.getDateStart());
        if (task.getDateFinish() != null) System.out.println("FINISHED: " + task.getDateFinish());
    }

}