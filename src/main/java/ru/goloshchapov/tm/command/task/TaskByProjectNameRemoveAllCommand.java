package ru.goloshchapov.tm.command.task;

import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskByProjectNameRemoveAllCommand extends AbstractTaskCommand{

    public static final String NAME = "task-remove-all-by-project-name";

    public static final String DESCRIPTION = "Remove all tasks by project name";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE ALL TASKS FROM PROJECT]");
        System.out.println("ENTER PROJECT NAME");
        final String name = TerminalUtil.nextLine();
        final List<Task> tasks = serviceLocator.getProjectTaskService().removeAllByProjectName(userId, name);
        if (tasks == null) throw new TaskNotFoundException();
        else tasks.clear();
    }
}
