package ru.goloshchapov.tm.command.task;

import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class TaskByIdFinishCommand extends AbstractTaskCommand{

    public static final String NAME = "task-finish-by-id";

    public static final String DESCRIPTION = "Finish task by id";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER ID:");
        final String taskId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().finishOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
    }
}
