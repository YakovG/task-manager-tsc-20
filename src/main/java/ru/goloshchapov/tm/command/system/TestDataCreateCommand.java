package ru.goloshchapov.tm.command.system;

import ru.goloshchapov.tm.command.AbstractCommand;

public final class TestDataCreateCommand extends AbstractCommand {

    public static final String NAME = "create-test-data";

    public static final String DESCRIPTION = "Create test dataset";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("CREATING TEST DATASET");
        serviceLocator.getProjectTaskService().createTestData();
    }
}
