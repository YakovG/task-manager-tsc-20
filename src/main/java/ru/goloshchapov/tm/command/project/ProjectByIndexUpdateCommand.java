package ru.goloshchapov.tm.command.project;

import ru.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.goloshchapov.tm.exception.entity.ProjectNotUpdatedException;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class ProjectByIndexUpdateCommand extends AbstractProjectCommand{

    public static final String NAME = "project-update-by-index";

    public static final String DESCRIPTION ="Update project by index";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = serviceLocator.getProjectService().findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = serviceLocator.getProjectService().updateOneByIndex(userId, index,name,description);
        if (projectUpdated == null) throw new ProjectNotUpdatedException();
    }
}
