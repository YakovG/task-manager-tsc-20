package ru.goloshchapov.tm.command.project;

import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.exception.entity.ProjectNotUpdatedException;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectByIdChangeStatusCommand extends AbstractProjectCommand{

    public static final String NAME = "project-change-status-by-id";

    public static final String DESCRIPTION ="Change project status by id";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        final Status[] statuses = Status.values();
        System.out.println(Arrays.toString(statuses));
        final String statusChange = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().changeOneStatusById(userId, projectId, statusChange);
        if (project == null) throw new ProjectNotUpdatedException();
    }
}
