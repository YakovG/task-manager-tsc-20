package ru.goloshchapov.tm.exception.entity;

import ru.goloshchapov.tm.exception.AbstractException;

public class UserByIdNotFoundException extends AbstractException {

    public UserByIdNotFoundException(final String message) {
        super("Error! User with ID: " + message + " not found...");
    }

}
